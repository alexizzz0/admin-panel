const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.scripts([
  'resources/assets/js/scrollto.js'
], 'public/la-assets/js/alx.js');

mix.less('resources/assets/less/alx/main.less', 'public/la-assets/css')
   .less('resources/assets/less/bootstrap/bootstrap.less', 'public/la-assets/css')
   .less('resources/assets/less/admin-lte/AdminLTE.less', 'public/la-assets/css');

mix//.sass('resources/assets/sass/login.scss', 'public/la-assets/css')
   //.sass('resources/assets/sass/main.scss', 'public/la-assets/css');
   .sass('resources/assets/sass/googlefonts/source-sans-pro.scss', 'public/la-assets/css');
