# LaraCMSAdmin

An Easy to use Easy to Install and Easy to Configure; Admin Panel based on LaraAdmin. Updated to work with Less.js Sass-Node on a Laravel Framework.

## Getting Started
[![Build Status](https://img.shields.io/badge/Install-Composer-lightgray?style=flat-square&logo=composer)](https://getcomposer.org/)  
[![Build Status](https://img.shields.io/badge/Install-NPM-lightgray?style=flat-square&logo=Npm)](https://www.npmjs.com/)  
[![Build Status](https://img.shields.io/badge/Install-Node.js-lightgray?style=flat-square&logo=node.js)](https://nodejs.org/en/)  
[![Build Status](https://img.shields.io/badge/Install-LessCss-lightgray?style=flat-square&logo=javascript)](https://getcomposer.org/)

### Prerequisites

* PHP >= 7.1
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Composer
* MySQL


Unix(Mac & Ubuntu) Installation for other OS please use their reference guides.


#### INSTALL PHP 7.1

##### Remove all previous versions of php
```
sudo apt-get purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "`
```
##### Add the repo
```
sudo add-apt-repository ppa:ondrej/php
```
##### Run Update
*the -y flag skips any agree promtps*
```
sudo apt-get update -y
```
##### Install PHP 7.1
*tested and deployed @ 7.1. Laravel 5.2 does not work past php7.1*
```
sudo apt-get install php7.1
```
##### Install Modules
*Modules can be installed individually or all in one command*  
*Here are the ones I recommend to install together*
*Other modules may have been depreciated so a version for 7.1 may not be available*
```
sudo apt-get install -y php7.1 libapache2-mod-php7.1 php7.1-cli php7.1-common php7.1-mbstring php7.1-gd php7.1-intl php7.1-xml php7.1-mysql php7.1-mcrypt php7.1-zip
```
##### Edit dir.conf in Apache
*Look for index.php and move it to the front of the configuration directive*
```
sudo nano /etc/apache2/mods-enabled/dir.conf
```
##### Setup your PHP
*Set your servers time zone*  
-America/New_York  
*Set a maximum upload size*  
-2mb is the default set in the PHP.ini  
```
sudo nano /etc/php/7.1/apache2/php.ini
```
##### Install CURL and the CURL php Module
*CURL is always a good idea to install as you may need it or an application may require it to run properly*
```
sudo apt-get install curl
sudo apt-get install php7.1-curl
```
#### INSTALL COMPOSER
*Dependency*
##### Unzip
```
sudo apt-get install git unzip
```
##### CD to your home directory
```
cd ~
```
##### Download
```
curl -sS https://getcomposer.org/installer -o composer-setup.php
```
##### Verify
*Copy the latest HASH and insert it on the link to verify*  
[![Build Status](https://img.shields.io/badge/Verify-Composer-green?style=flat-square&logo=composer)](https://composer.github.io/pubkeys.html)
```
php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```
##### Install
```
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```
##### Make Global
```
mv composer.phar /usr/local/bin/composer
```
##### Verify Install
```
composer
```
##### Edit $PATH
*MAC*
```
sudo nano .zsh
```
##### Add path to shell
```
export PATH=/usr/local/mysql/bin:$PATH
```
*Ubuntu*
```
sudo nano .bashrc
```
##### Add path to shell
```
export PATH="$PATH:$HOME/.config/composer/vendor/bin"
```
*Close terminal and reopen*

#### INSTALL MY MySQL
```
sudo apt-get install mysql-server
```
*Set Root Access then login using your favorite SQL Admin App.*  
*Create a new user for your application.*

## Install The Project
*cd to your servers root directory and clone the project from gitlab*

[![Build Status](https://img.shields.io/badge/DL-LaraCMSAdmin-orange?style=flat-square&logo=gitlab)](https://gitlab.com/alexizzz0/admin-panel.git)
```
git clone https://gitlab.com/alexizzz0/admin-panel.git
```

*cd into the admin-panel folder*
```
cd admin-panel
```

*Set permissions*
```
sudo chmod -R 777 storage/ bootstrap/ database/migrations/
```
*run installer*  
*This will add the super user for the project*  
If you want to use the gravatar icons make suer to enter your accounts email address.

## Deployment
```
php artisan la:install
```
*install project dependencies*
```
npm install
```

Add additional notes about how to deploy this on a live system

## Built With

* [![Build Status](https://img.shields.io/badge/DL-Laravel-red?style=flat-square&logo=laravel)](https://gitlab.com/alexizzz0/admin-panel.git) - Laravel 5.2.3
* [![Build Status](https://img.shields.io/badge/DL-Less-blue?style=flat-square&logo=javascript)](https://gitlab.com/alexizzz0/admin-panel.git) - Less 3.11
* [![Build Status](https://img.shields.io/badge/DL-Sass-ff69b4?style=flat-square&logo=sass)](https://gitlab.com/alexizzz0/admin-panel.git) - Sass 1.15

## Authors

* **LaraAdmin** - *Initial work* - [Dwij IT Solutions](https://github.com/dwijitsolutions/laraadmin)
* **Alex Sanchez** - *Rebuild work* - [AlxCompany](https://alx.company)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
