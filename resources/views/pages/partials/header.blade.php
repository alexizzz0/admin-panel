<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="{{ LAConfigs::getByKey('site_description') }}">
  <meta name="author" content="Alex Sanchez | Alx Company">

  <meta property="og:title" content="{{ LAConfigs::getByKey('sitename') }}" />
  <meta property="og:type" content="website" />
  <meta property="og:description" content="{{ LAConfigs::getByKey('site_description') }}" />

  <meta property="og:url" content="https://alx.company" />
  <meta property="og:sitename" content="Site Name" />
  <meta property="og:image" content="600x600.jpg" />

  <title>{{ LAConfigs::getByKey('sitename') }}</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('/la-assets/css/bootstrap.css') }}" rel="stylesheet" />

  <link href="{{ asset('la-assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Custom styles for this template -->
  <link href="{{ asset('/la-assets/css/main.css') }}" rel="stylesheet/less" />

  <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

  <script type="text/javascript" src="{{ asset('/la-assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/la-assets/js/alx.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/la-assets/js/less.js') }}" data-env="development" data-poll="10000"></script>
  <script>
    less.watch();
  </script>

</head>
