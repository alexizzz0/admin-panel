<section id="contact" name="contact">
  <div id="footerwrap">
    <div class="container">
      <div class="col-lg-5">
        <h3>Contact The Admin</h3><br>
        <div class="contact-link"><i class="fa fa-envelope-o"></i> <a href="mailto:alex.s@alx.company">alex.s@alx.company</a></div>
        <div class="contact-link"><i class="fa fa-cube"></i> <a href="https://alx.company">alx.company</a></div>
        <div class="contact-link"><i class="fa fa-phone"></i> <a href="tel:+1 336-710-5552">+1 (336) 710-5552</a></div>
      </div>

    </div>
  </div>
</section>
<footer>
  <div id="c">
    <div class="container">
      <p>
        <strong>Copyright &copy; {{date('Y')}}. Powered by <a href="https://alx.company"><b>The Alx Company</b></a>
      </p>
    </div>
  </div>
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/la-assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script>
  $('.carousel').carousel({
    interval: 3500
  })
</script>
