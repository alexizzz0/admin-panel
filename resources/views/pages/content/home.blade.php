@extends('pages.layouts.default')

@section('content')
<div id="top"></div>
<body>
  <!-- Fixed navbar -->
  <div id="navigation" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#top"><b>{{ LAConfigs::getByKey('sitename') }}</b></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#home">Home</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          @if (Auth::guest())
          <li><a href="{{ url('/login') }}">Login</a></li>
          @else
          <li><a href="{{ url(config('laraadmin.adminRoute')) }}">{{ Auth::user()->name }}</a></li>
          @endif
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>


  <section id="home">
    <div id="headerwrap">
      <div class="container middle">
        <div class="row centered">
          <div class="col-lg-12">
            <h1>{{ LAConfigs::getByKey('sitename_part1') }}
              <b>
                <a>{{ LAConfigs::getByKey('sitename_part2') }}</a>
              </b>
            </h1>
            <h3>{{ LAConfigs::getByKey('site_description') }}</h3>
          </div>
        </div>
      </div>
      <!--/ .container -->
    </div>
    <!--/ #headerwrap -->
  </section>

  @endsection
