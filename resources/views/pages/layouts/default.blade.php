<!DOCTYPE html>
  <html lang="en">

    @include('pages.partials.header')


    @yield('content')


    @include('pages.partials.footer')

  </body>
</html>
